'use strict';

require('./tasks/require-any.js');
require('./tasks/transpiler.js');

const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const rename = require('gulp-rename');
const rimraf = require('gulp-rimraf');
const run = require('run-sequence');

gulp.task('watch', ['watch-require-any', 'watch-transpiler']);

gulp.task('deploy-prepare', ['clean-require-any', 'build-require-any', 'clean-transpiler', 'build-transpiler'], function() {
	
});

gulp.task('deploy-all', function() {
	return gulp.src(['dist/require-any.js', 'dist/transpiler-*.js'])
		.pipe(concat('require-any-all.js'))
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(rename(function(path) { path.extname = '.min.js' }))
		.pipe(gulp.dest('dist'))
		;
});

gulp.task('deploy-babel', function() {
	return gulp.src(['dist/require-any.js', 'dist/transpiler-babel.js'])
		.pipe(concat('require-any-babel.js'))
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(rename(function(path) { path.extname = '.min.js' }))
		.pipe(gulp.dest('dist'))
		;
});

gulp.task('deploy-coffee', function() {
	return gulp.src(['dist/require-any.js', 'dist/transpiler-coffee.js', 'dist/transpiler-coffee-react.js'])
		.pipe(concat('require-any-coffee.js'))
		.pipe(gulp.dest('dist'))
		.pipe(uglify())
		.pipe(rename(function(path) { path.extname = '.min.js' }))
		.pipe(gulp.dest('dist'))
		;
});

gulp.task('deploy', ['clean-require-any', 'build-require-any', 'clean-transpiler', 'build-transpiler'], function(fn) {
	run(['deploy-all', 'deploy-babel', 'deploy-coffee'], fn)
});

//gulp.task('clean', function (fn) {
//	var src = [
//		'dist/require-any.js',
//		'dist/require-any.js.map'
//	];
//	return gulp.src(src).pipe(rimraf());
//});
//
//gulp.task('build', function () {
//	return gulp.src('src/*.es6')
//		.pipe(sourcemaps.init())
//		.pipe(babel())
//		.on('error', gutil.log)
//		.pipe(sourcemaps.write('.'))
//		.pipe(gulp.dest('dist'))
//		;
//});
//
//gulp.task('watch', ['build'], function () {
//	return gulp.watch('src/*.es6', ['build']);
//});

