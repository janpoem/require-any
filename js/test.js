(function (global) {

	var rjsConfig = {
		//baseUrl: './js',
		paths  : {
			'react'            : 'bower_components/react/react',
			'react-dom'        : 'bower_components/react/react-dom',
			'babel-standalone' : 'bower_components/babel-standalone/babel.min',
			'coffee-script'    : 'bower_components/coffee-script/extras/coffee-script',
			// 'coffee-react'     : 'src/transpiler/coffee-react',
			// 指定加载的协议，你可以指定任意名称，比如load
			'any'              : 'dist/require-any-all.min',
			// coffee-script 对译器
			// 'transpiler-coffee': 'dist/require-any-all.min',
			// babel 对译器
			// 'transpiler-babel' : 'dist/require-any-all.min',
			'test2'            : 'test/Test2'
		},
		config : {
			any: {
				// 是否编译模式，如果是编译模式，则直接加载这个模块的js文件
				// any!test.jsx，当build = true，则会去加载 test.js 文件
				build  : false,
				// 是否显示调试的信息
				debug  : 1,
				// 声明相关的，插件
				plugins: {
					coffee: 'transpiler-coffee',
					es6   : 'transpiler-babel'
				},
				// 后缀文件名的别名，指定用哪个插件来进行处理
				ext    : {
					cjsx: 'coffee',
					jsx : 'es6'
				},
				// 后缀转换（编译）时的参数，以后缀名为指向。
				// 如果这里指定了jsx，则优先取jsx的配置。
				// 如果没指定jsx，则会加载es6的配置。
				options: {
					es6: {
						plugins: [
							"transform-es2015-modules-amd",
							"transform-es2015-block-scoping",
							"transform-class-properties",
							"transform-es2015-computed-properties"
						]
					}
				}
			}
		}

	};

	rjsConfig.urlArgs = "version=" + (new Date()).valueOf();

	requirejs.config(rjsConfig);

})(this);
