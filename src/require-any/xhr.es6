//====xhr.es6====//


const xhrMethods = {GET: 'GET', POST: 'POST', DELETE: 'DELETE', PUT: 'PUT'}
	, xhrDefaultMethod = 'GET';

function xhrFilterMethod(method) {
	return (method && xhrMethods[upper.call(method)]) || xhrDefaultMethod;
}

function initXhrOptions(url, options) {
	options = options || {};

	options.url = url;
	options.method = xhrFilterMethod(options.method);
	options.async = isUndefined(options.async, true);

	return options;
}

function initXhr(url, options) {
	if (typeof XMLHttpRequest === 'undefined' || XMLHttpRequest === null)
		throw new Error('Your browser unsupported XMLHttpRequest!');
	options = initXhrOptions(url, options);
	var xhr = new XMLHttpRequest()
		, isError = false, isAbort = false
		;
	xhr.open(options.method, options.url, options.async);
	xhr.onerror = function (event) {
		isError = true;
		callFunc(xhr, options.error, new Error('Network Error', url));
	};
	xhr.onabort = function (event) {
		isAbort = true;
		callFunc(xhr, options.abort);
	};
	xhr.onload = function (event) {
		if (!isAbort && !isError) {
			callFunc(xhr, options.done);
		}
	};
	xhr.onreadystatechange = function (event) {
		if (!isAbort && !isError) {
			if (xhr.status > 399 && xhr.status < 600) {
				isError = true;
				callFunc(xhr, options.error, new Error('HTTP status: ' + xhr.status, url));
			}
			else {
				switch (xhr.readyState) {
					case 2 :
						callFunc(xhr, options.headers);
						break;
					case 3 :
						callFunc(xhr, options.loading);
						break;
				}
			}
		}
	};
	xhr.send(null);
	return xhr;
}
