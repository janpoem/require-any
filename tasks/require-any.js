'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const rename = require('gulp-rename');
const rimraf = require('gulp-rimraf');
const run = require('run-sequence');

const requireAnySrcFiles = [
	'src/require-any/base64.es6',
	'src/require-any/utils.es6',
	'src/require-any/xhr.es6',
	'src/require-any/cached.es6',
	'src/require-any/loader.es6',
	'src/require-any/exports.es6'
];

const requireAnyBabelOptions = {
	'presets': ['es2015'],
	'plugins': [
		'transform-es2015-modules-amd',
		'babel-plugin-transform-class-properties',
		'transform-es2015-block-scoping'
	]
};

const requireAnyDistFiles = [
	'dist/require-any.js',
	'dist/require-any.js.map',
	'dist/require-any.min.js'
];

gulp.task('clean-require-any', function () {
	return gulp.src(requireAnyDistFiles, { read: false })
		.pipe(rimraf({ force: true }));
});

gulp.task('build-require-any', function () {
	return gulp.src(requireAnySrcFiles)
		.pipe(sourcemaps.init())
		.pipe(concat('require-any.js'))
		.pipe(babel(requireAnyBabelOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('dist'));
});

gulp.task('deploy-require-any', ['clean-require-any', 'build-require-any'], function(fn) {
	return gulp.src(requireAnySrcFiles)
		.pipe(concat('require-any.min.js'))
		.pipe(babel(requireAnyBabelOptions))
		.pipe(uglify())
		.pipe(gulp.dest('dist'));
});

gulp.task('watch-require-any', ['build-require-any'], function() {
	return gulp.watch(requireAnySrcFiles, ['build-require-any']);
});