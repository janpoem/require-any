'use strict';

const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rimraf = require('gulp-rimraf');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const gutil = require('gulp-util');
const rename = require('gulp-rename');

const transpilerSrcFiles = 'src/transpiler/*.js';

const transpilerDistFiles = 'dist/transpiler-*';

const babelOptions = {
	'presets': ['es2015'],
	'plugins': [
		'transform-es2015-modules-amd',
		'babel-plugin-transform-class-properties',
		'transform-es2015-block-scoping'
	]
};

gulp.task('clean-transpiler', function () {
	return gulp.src(transpilerDistFiles, { read: false })
		.pipe(rimraf({ force: true }));
});

gulp.task('build-transpiler', function () {
	return gulp.src(['src/transpiler/*.js'])
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('.'))
		.pipe(rename(function(path) { path.basename = 'transpiler-' + path.basename; }))
		.pipe(gulp.dest('dist'));
});

gulp.task('watch-transpiler', ['build-transpiler'], function() {
	gulp.src(transpilerSrcFiles, ['build-transpiler']);
});