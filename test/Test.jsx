var React = require('react');


module.exports = React.createClass({
	displayName: 'Test',
	name: 'Test',
	getInitialState: function() {
		return {
			count: 0
		}
	},
	click: function(event) {
		this.setState({ count: this.state.count + 1 });
	},
	render: function () {
		return (
			<button onClick={this.click}>
				{this.name} + {this.state.count}
			</button>
		);
	}
});
